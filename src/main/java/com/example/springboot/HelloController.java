package com.example.springboot;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@GetMapping("/{title}")
	public String index(@PathVariable String title) {
		return "Greetings " + title + " from Spring Boot!";
	}

}
